package com.airline.storecustomermanager.service;

import com.airline.storecustomermanager.entity.Information;
import com.airline.storecustomermanager.model.InformationItem;
import com.airline.storecustomermanager.repository.InformationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InformationService {
    private final InformationRepository informationRepository;

    public void setInformation(Boolean isMan, String buy,  Integer age) {
        Information addData = new Information();
        addData.setIsMan(isMan);
        addData.setBuy(buy);
        addData.setAge(age);

        informationRepository.save(addData);
    }
    public List<InformationItem> getInformation() {
        List<InformationItem> result = new LinkedList<>();
        List<Information> originData = informationRepository.findAll();

        for (Information item : originData) {
            InformationItem addData = new InformationItem();
            addData.setIsMan(item.getIsMan());
            addData.setBuy(item.getBuy());
            addData.setAge(item.getAge());

            result.add(addData);
        }
        return result;

    }
}
