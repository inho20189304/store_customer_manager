package com.airline.storecustomermanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InformationRequest {
    private Boolean isMan;
    private String buy;
    private Integer age;

}
