package com.airline.storecustomermanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InformationItem {
    private Boolean isMan;
    private String buy;
    private Integer age;

}
