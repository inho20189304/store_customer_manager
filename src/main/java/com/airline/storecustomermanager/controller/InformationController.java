package com.airline.storecustomermanager.controller;

import com.airline.storecustomermanager.entity.Information;
import com.airline.storecustomermanager.model.InformationItem;
import com.airline.storecustomermanager.model.InformationRequest;
import com.airline.storecustomermanager.service.InformationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/information")
public class InformationController {
    private final InformationService informationService;

    @PostMapping("/data")
    public String setInformation(@RequestBody InformationRequest request) {
        informationService.setInformation(request.getIsMan(), request.getBuy(), request.getAge());

        return "ok";
    }
    @GetMapping("/bring")
    public List<InformationItem> getInformation() {
        List<InformationItem> result = informationService.getInformation();
        return result;
    }

}
